#!/usr/bin/env python3

import json
import re
import unittest


class PostalCodeValidationTest(unittest.TestCase):
    REGEXES = json.load(open('regexes.json'))

    def check(self, country, valid_codes, invalid_codes):
        regex = re.compile(self.REGEXES[country])

        for code in valid_codes:
            self.assertIsNotNone(re.fullmatch(regex, code), "{}".format(code))

        for code in invalid_codes:
            self.assertIsNone(re.fullmatch(regex, code), "{}".format(code))

    def test_FR(self):
        self.check('FR', ('01111',), ('00111',))

    def test_GB(self):
        valid_codes = (
            'EC1A 1BB', 'W1A 0AX', 'M1 1AE', 'B33 8TH', 'CR2 6XH', 'DN55 1PT',
            'BFPO 1', 'BFPO 123', 'BFPO 1234',
            # Akrotiri and Dhekelia
            'BFPO 57', 'BF1 2AT', 'BFPO 58', 'BF1 2AU',
        )

        invalid_codes = (
            # incorrect prefix
            '1A1 1AA', '1A 1AA', '1AA 1AA', '1A11 1AA', '1AA1 1AA',
            'A11A 1AA', 'A1AA 1AA',
            # incorrect suffix
            'A1 AAA', 'A1 11A', 'A1 1A1', 'A1 111',
            # incorrect BFPO
            'BFPO 12345',
        )

        self.check('GB', valid_codes, invalid_codes)

    def test_UA(self):
        self.check('UA', ('01234',), ('00123',))


if __name__ == '__main__':
    unittest.main()

# Postal code format regexes

This project is a compilation of regexes by country, to validate the *format* of a postal code.

This means that all valid postal codes are matched but a postal code can be matched without being (currently) assigned to an existing place.
Also, all rejected postal codes will never be valid (unless the country’s postal code system changes obviously, but this happens very rarely).

This has several advantages:

- simple and low-maintenance
- small and fast
- forward-compatible

## Sources

- [Deprecated data from CLDR 27](https://github.com/unicode-cldr/cldr-core/blob/6d91edcb3c73ad4c93576b9b712379c40a2007c8/supplemental/postalCodeData.json)
- FR (France): https://en.wikipedia.org/wiki/Postal_codes_in_France
- GB (United Kingdom): https://en.wikipedia.org/wiki/Postcodes_in_the_United_Kingdom
- GI (Gibraltar): https://en.wikipedia.org/wiki/Postal_addresses_in_Gibraltar
- IE (Republic of Ireland): https://en.wikipedia.org/wiki/Postal_addresses_in_the_Republic_of_Ireland
- KY (Cayman Islands): https://en.wikipedia.org/wiki/Postal_codes_in_the_Cayman_Islands
- NL (Netherlands): https://en.wikipedia.org/wiki/Postal_codes_in_the_Netherlands
- UA (Ukraine): https://en.wikipedia.org/wiki/Postal_codes_in_Ukraine
- VG (British Virgin Islands): http://www.bvi.gov.vg/content/what-are-postcodes-addresses-british-virgin-islands
